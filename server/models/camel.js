var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var camelModel = new Schema({
    color: { type: String },
    position: { type: Number }
});

module.exports = mongoose.model('camels', camelModel);
